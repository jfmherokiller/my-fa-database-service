import * as ExtremeSql from "../DatabaseFunctions";
import * as sqlite from "sqlite";
import * as express from "express";
import {DedupfAFolder} from "../dedupFiles";

const router = express.Router();
const DatabasePath = "./data/furryart_data.db";
let dbPromise = sqlite.open(DatabasePath);


router.get("/SearchType/:Rtype/Query/:query/", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    await ExtremeSql.InnerBody(true, dbPromise, req.params.Rtype, req, res, next);
});
router.get("/SearchType/:Rtype/Query/:query/total/:total/offset/:offset", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    await ExtremeSql.InnerBody(false, dbPromise, req.params.Rtype, req, res, next);
});


router.get("/FaDatabase", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    res.render("DatabaseControls");
});
router.post("/UpdateTheFaDatabase", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    const db = await dbPromise;
    dbPromise = null;
    dbPromise = await ExtremeSql.UpdateDatabaseExt(db);
});
router.post("/RebuildFaDatabaseFromScratch", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    const db = await dbPromise;
    dbPromise = null;
    dbPromise = await ExtremeSql.createDBForServer(db);
});
router.post("/DedupeFiles", async function (req, res, next) {
    if (dbPromise === null) {
        ExtremeSql.Failed(res);
        return;
    }
    const ThePromise = dbPromise;
    dbPromise = null;
    await DedupfAFolder();
    dbPromise = ThePromise;
});
export = router;