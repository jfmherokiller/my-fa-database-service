import * as createError from 'http-errors';
import * as express from "express";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as sassMiddleware from "node-sass-middleware";
import * as HttpError from "standard-http-error";
import * as indexRouter from "./routes/index";
import * as usersRouter from "./routes/users";
import * as FADb from "./routes/FADb";
import {ProcessFurryFile} from "./WebpageInjections";

const app = express();

// view engine setup
const depLinker = require('dep-linker');
depLinker.linkDependenciesTo('./public/scripts')
    .then(() => console.log('Finished.'));


app.use(function(err, req, res, next) {
    if (!(err instanceof HttpError)) return void next(err);

    res.statusCode = err.code;
    res.statusMessage = err.message;
    res.render("error", {title: err.message})
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('view cache',true);
app.set('etag', 'strong');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true,
    prefix:"/public"
}));
app.use("/public",express.static(path.join(__dirname, 'public')));

app.use("/",indexRouter);
app.use('/db', FADb);
//app.use('/furryart', express.static('E:/furryart/files/',{immutable:true}));
app.use('/furryartArchive', express.static('E:/websites/Fa_archive/',{immutable:true}));
app.use('/ServerJs', express.static('ServerJs',{maxAge:31536000,immutable: true}));
app.get('/furryart/:file', function(req, res,next) {
    if(!req.params.file.endsWith(".html")) {
        res.sendFile('E:/furryart/files/' + req.params.file,{immutable:true});
    } else {
        const Outfile =ProcessFurryFile(req.params.file);
        res.send(Outfile);
    }
});
//app.get('/furryart', express.static('E:/furryart/files/',{immutable:true}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


export = app;
