let fs = require("fs");
let Streamme = fs.createWriteStream('ServerJs/wordme.js');
let browserifyConfig = {
    standalone:"word-extractor",
};
let browserify = require('browserify');
let b = browserify(browserifyConfig);
b.add('./worldFile.js');
b.bundle().pipe(Streamme);