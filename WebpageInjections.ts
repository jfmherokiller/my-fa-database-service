import * as fs from "fs";
import * as cheerio from "cheerio";

const myScript= `
(function () {
    'use strict';
    const topOfPage = document.querySelector(".alt1");
    async function InsertLink(ImageNode) {
        const { content } = document.head.querySelector("[property~='og:url'][content]");
        const postUrl = content;
        const MyLink = document.createElement('a');
        MyLink.href = postUrl;
        MyLink.innerHTML = postUrl;
        MyLink.style.fontSize = "12pt";
        topOfPage.append(MyLink);
    }
    async function GetImageUrl() {
        const listOfImageExt = [".png", ".jpeg", ".jpg", ".gif"];
        const pageUrl = window.location.href;
        const ListOfPossibleUrls = listOfImageExt.map(extent => {
            return fetch(pageUrl.replace("_desc.html", extent));
        });
        let CorrectUrl;
        for await (const fetchedUrl of ListOfPossibleUrls) {
            if (fetchedUrl.ok) {
                CorrectUrl = fetchedUrl.url;
            }
        }
        return CorrectUrl;
    }
    async function AppendImageToPage() {
        const myimage = document.createElement('img');
        myimage.src = await GetImageUrl();
        myimage.style.maxWidth = "379px";
        myimage.style.maxHeight = "auto";
        topOfPage.prepend(myimage);
        topOfPage.prepend(document.createElement("br"));
        return myimage;
    }
    AppendImageToPage().then(InsertLink);
    // Your code here...
})();`;

function ProcessFurryFile(TheFile) {
    const html = fs.readFileSync('E:/furryart/files/' + TheFile, 'utf8');
    const $ = cheerio.load(html);
    const scriptNode = `<script>${myScript}</script>`;
    $('body').append(scriptNode);
    return $.html();
}

export {ProcessFurryFile}