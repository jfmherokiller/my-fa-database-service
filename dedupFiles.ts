/*
Get-ChildItem -Recurse -File -Path "E:\furryart\files" -OutBuffer 1000 | Where-Object -Property Name -match '^\d+_\d+\.' | ForEach-Object {
    $realname = "E:\furryart\files\" + ($_.Name.Split("_", 2)[1]);
    $realname | Out-Host
    Move-Item -force $_ -Destination $realname
}*/

import * as fs from "fs";
import * as path from "path";

const fsPromise = fs.promises;
const FilePrefix = RegExp(/^\d+_\d+\./);
const FileRemovalPrefix = RegExp(/^\d+_/);
const FileSpecialChars = RegExp(/[\]\[]+/);
const PathPortion = "E:\\furryart\\files\\";

async function DedupfAFolder() {
    let DirectoryContents: string[] = await fsPromise.readdir(PathPortion);
    await RemoveSpecialChars(DirectoryContents);
    DirectoryContents = DirectoryContents.filter(File => {
        return FilePrefix.test(File);
    });
    if (DirectoryContents.length === 0) {
        return;
    }
    console.log(DirectoryContents);
    DirectoryContents.forEach(File => {
        let Basefile = path.join(PathPortion, File);
        let RenamedFiles = path.join(PathPortion, File.replace(FileRemovalPrefix, ""));
        fsPromise.rename(Basefile, RenamedFiles);
    });

}
async function RemoveSpecialChars(Directory: string[]) {
    let FilteredDirectory = Directory.filter(File => {
        return FileSpecialChars.test(File);
    });
    if (FilteredDirectory.length === 0) {
        return;
    }
    FilteredDirectory.forEach(File => {
        let Basefile = path.join(PathPortion, File);
        let RenamedFiles = path.join(PathPortion, File.replace(FileSpecialChars, ""));
        fsPromise.rename(Basefile, RenamedFiles);
    })
}

export {DedupfAFolder}