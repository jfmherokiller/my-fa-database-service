import to from "await-to-js";

const sqlite = require('sqlite');
const fs = require("fs");
const path = require("path");
const cheerio = require("cheerio");
const sw = require("stopword");
const SQL = require("sql-template-strings");

const fsPromises = fs.promises;
const DatabasePath = "./data/furryart_data.db";

async function HandleTables(db) {
    // await db.run(`CREATE TABLE IF NOT EXISTS
    //     Images(
    //     Image_Id INTEGER PRIMARY KEY,
    //     Image_file_name TEXT,
    //     Date_published INTEGER,
    //     category TEXT,
    //     theme TEXT,
    //     species TEXT,
    //     Gender TEXT,
    //     rating TEXT,
    //     Tags TEXT
    //      )`);
    await db.run(`CREATE VIRTUAL TABLE IF NOT EXISTS FurryArt USING fts5(Image_file, Description);`)
}

async function UpdateDatabaseExt(db) {
    await UpdateDatabase(db);
    db.close();
    return await GenerateNewDbPromise();
}

async function GenerateNewDbPromise() {
    return sqlite.open("./data/furryart_data.db");
}

//#createTableStructure
async function createDB() {
    const db = await GenerateNewDbPromise();

    await HandleTables(db);
    //await AddFurryArtData2(db);
    await UpdateDatabase(db);
    await db.close();
}

//#createTableStructure
async function createDBForServer(db) {
    await db.close();
    if (fs.existsSync(DatabasePath)) {
        fs.unlinkSync(DatabasePath);
    }
    const NewDbp = await GenerateNewDbPromise();
    db = await NewDbp;
    await HandleTables(db);
    await UpdateDatabase(db);
    await db.close();
    return await GenerateNewDbPromise();
}

async function FilterDirectoryAgenstDb(db) {
    let Results = await db.all(SQL`SELECT Image_File From FurryArt`);
    Results = new Set(Results.map(function (Item) {
        return Item.Image_file
    }));
    const Directory_Results = new Set((await fsPromises.readdir("E:\\furryart\\files")).filter(f => !(f.endsWith(".html"))));
    const NotFoundInDatabase = new Set([...Directory_Results].filter((x) => !Results.has(x)));
    const NotFoundInDir = new Set([...Results].filter((x) => !Directory_Results.has(x)));

    return [Array.from(NotFoundInDatabase), Array.from(NotFoundInDir)];
}

async function UpdateDatabase(db) {
    const RealResults = await FilterDirectoryAgenstDb(db);
    await RemoveNonExistantRowsFromDb(RealResults[1], db);
    if (RealResults[0].length === 0) {
        return;
    }
    let RealFiles = await PrepareFileHtmlArray(RealResults[0]);
    let FurryArtDbUpdate = await AddFurryArtData2(db, RealFiles);
}

async function RemoveNonExistantRowsFromDb(Files: string[], db) {
    if(Files.length ===0) {
        return;
    }
    for await (let File of Files) {
        const TheQuery = `DELETE FROM FurryArt WHERE Image_file = '${File}';`;
        const result = await db.run(TheQuery);
    }
}

async function ProduceOtherBits(Files) {
    const FilesAndPossibleDescriptions = Files.map(File => `${File};E:\\furryart\\files\\${path.parse(File).name}_desc.html`);
    return FilesAndPossibleDescriptions;
}

async function FigureOutIfHtmlFilesExist(Files) {
    return Files.filter(f => fs.existsSync(f.split(";")[1]))
}

async function PrepareFileHtmlArray(Files) {
    let UnfilteredFiles = await ProduceOtherBits(Files);
    let FilteredFiles = await FigureOutIfHtmlFilesExist(UnfilteredFiles);
    return FilteredFiles;
}

async function ProcessHtmlFile(FileInside, SpecialList) {
    const PageText = cheerio.load(FileInside).root().text();
    const NoLineBreaks = PageText.replace(/(\r\n|\n|\r)/gm, " ");
    const ReducesSpaces = NoLineBreaks.replace(/\s+/g, ' ');
    const LowerCased = ReducesSpaces.split(" ").map(local => local.toLocaleLowerCase()).join(" ");
    const RemoveSpecialWordsAndEmpyBits = LowerCased.split(" ").filter(Item => Item !== "").join(" ");
    const ToSet = new Set(RemoveSpecialWordsAndEmpyBits.split(" "));
    const RemovedStopWords = sw.removeStopwords(Array.from(ToSet), SpecialList).join(" ");
    return RemovedStopWords;
}

async function AddFurryArtData2(db, Files) {
    const SpecialList = await GenerateWordList();
    for await (const file of Files) {
        const FileInside = await fsPromises.readFile(file.split(";")[1], {encoding: "utf8"});
        try {
            const FileData = path.parse(file.split(";")[0]).base;
            const processed_description = await ProcessHtmlFile(FileInside, SpecialList);
            const Query = SQL`INSERT INTO FurryArt(Image_file, Description) VALUES (${FileData},${processed_description})`;
            const query = await db.run(Query);
        } catch (e) {
            console.log(e);
        }
    }
}

async function GenerateWordList() {
    let SpecialList = sw.en;
    const ExtraParts = ["species:", "keywords:", "rating", "gender:", "favorites:", "comments:", "views:", "theme:", "image", "specifications:", "submission", "information:", "category:"];
    SpecialList = SpecialList.concat(ExtraParts);
    return SpecialList;
}

/*async function AddFurryArtData(db) {
    let HtmlFiles = (await fsPromises.readdir("E:\\furryart\\files")).filter(Item => !Item.endsWith(".html")).map(function (Item) {
        return `${Item};E:\\furryart\\files\\${path.parse(Item).name}_desc.html`;
    }).filter(Item => fs.existsSync(Item.split(";")[1]));
    for await (const file of HtmlFiles) {
        let FileInside = await fsPromises.readFile(file.split(";")[1], {encoding: "utf8"});
        const FileObject = new FileDataObject(FileInside);
        const QueryToRUN = `INSERT INTO
        Images(Image_file_name,Date_published,category,theme,species,Gender,rating,Tags)
        VALUES ('${path.parse(file.split(";")[0]).base}',${FileObject.PostDate},'${FileObject.Category}','${FileObject.Theme}','${FileObject.Species}','${FileObject.Gender}','${FileObject.Rating}','${escape(FileObject.Keywords)}')`;
        try {
            const query = await db.run(QueryToRUN);
        } catch (e) {
            console.log(e);
            console.log(QueryToRUN);
        }
    }
}*/
async function HandleQueryMulti(db, req, next, Filter) {
    let SecondaryResults;
    let Results = [];

    let Query = `SELECT Image_file FROM FurryArt( '${req.params.query}') WHERE Image_file ${Filter} order by rank Limit ${req.params.total} offset ${req.params.offset};`;
    SecondaryResults = await to(db.all(Query),);
    if (SecondaryResults[0] === null) {
        Results = SecondaryResults[1];
    } else {
        console.log(Query);
        console.log(SecondaryResults[0]);
    }

    return Results;
}

async function HandleQueryRunner(db, req, next, type) {
    let Ressults;
    const FilterToImages = `like '%.jpg' or Image_file like '%.png' or Image_file like '%.gif' or Image_file like '%.jpeg'`;
    const FilterToStories = `like '%.txt' or Image_file like '%.doc' or Image_file like '%.docx' or Image_file like '%.rtf' or Image_file like '%.pdf' or Image_file like '%.odt'`;
    let Filter;
    if (type === "images") {
        Filter = FilterToImages;
    }
    if (type === "stories") {
        Filter = FilterToStories;
    }
    if (req.params.total === undefined) {
        Ressults = await HandleQuerySingle(db, req, next, Filter);
    } else {
        Ressults = await HandleQueryMulti(db, req, next, Filter)
    }
    return Ressults;
}

async function HandleQuerySingle(db, req, next, Filter) {
    let SecondaryResults;
    let Results = [];

    let Query = `SELECT Image_file FROM FurryArt( '${req.params.query}') WHERE Image_file ${Filter} order by rank;`;
    SecondaryResults = await to(db.all(Query));
    if (SecondaryResults[0] === null) {
        Results = SecondaryResults[1];
    } else {
        console.log(Query);
        console.log(SecondaryResults[0]);
    }
    return Results;
}

async function InnerBody(SingleOrDouble, dbPromise, type, req, res, next) {
    const db = await dbPromise;
    let Ressults;
    if (SingleOrDouble) {
        Ressults = await HandleQueryRunner(db, req, next, type);
        if (Ressults.length !== 0) {
            res.render('FADb', {
                MyResults: Ressults,
                MyQuery: req.params.query,
                Mylimit: 100,
                Myoffset: 0,
                Searchtype: `${type}`,
            })
        } else {
            Failed(res);
        }
    } else {
        Ressults = await HandleQueryRunner(db, req, next, type);
        if (Ressults.length !== 0) {
            res.render('FADb', {
                MyResults: Ressults,
                MyQuery: req.params.query,
                Mylimit: req.params.total,
                Myoffset: req.params.offset,
                Searchtype: `${type}`,
            })
        } else {
            Failed(res);
        }
    }
}

function Failed(res) {
    res.statusCode = '451';
    res.statusMessage = "Unavailable For Legal Reasons";
    res.locals.error = {status: 451, stack: ""};
    //res.set("Link", "<https://spqr.example.org/legislatione>; rel=\"blocked-by\"");
    res.render("error", {
        message: '<html>\n' +
            '      <head><title>Unavailable For Legal Reasons</title></head>\n' +
            '      <body>\n' +
            '            <h1>Unavailable For Legal Reasons</h1>\n' +
            '            <p>This request may not be serviced in the Roman Province\n' +
            '            of Judea due to the Lex Julia Majestatis, which disallows\n' +
            '            access to resources hosted on servers deemed to be\n' +
            '            operated by the People\'s Front of Judea.</p>\n' +
            '     </body>\n' +
            '</html>'
    });
}


export {UpdateDatabaseExt, createDBForServer, HandleQueryMulti, InnerBody, Failed}